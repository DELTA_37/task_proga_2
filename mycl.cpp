#include "mycl.h"

int mistake = 0;

CVector::CVector(int n)
{
	this->dim = n;
	this->v = new double[n];
}

CVector::~CVector(void)
{
	this->Clean();
}

CVector::CVector(void)
{
	this->SetZero();
}

CVector::CVector(const CVector &p)
{
	this->SetZero();
	this->CopyOnly(p);
}

void CVector::SetZero(void)
{
	this->dim = 0;
	this->v = NULL;
}

void CVector::CopyOnly(const CVector & v)
{
	int i;
	this->dim = v.dim;
	this->v = new double[v.dim];
	for (i = 0; i <= v.dim - 1; i++)
	{
		this->v[i] = v.v[i];
	}
}

void CVector::Clean(void)
{
	delete[] this->v;
	int dim = 0;
}

CVector & CVector::operator=(const CVector &pp)
{
	CVector *g = new CVector(pp);
	int i;
	this->dim = g->dim;
	this->v = new double[g->dim];
	for (i = 0; i <= g->dim - 1; i++)
	{
		this->v[i] = g->v[i];
	}
	return *g;
}


CVector::CVector(int n, const double *mas)
{
	int i;
	this->dim = n;
	this->v = new double[n];
	for (i = 0; i <= n - 1; i++)
	{
		this->v[i] = mas[i];
	}
}

CVector & CVector::operator+(const CVector &pp)
{
	int i;
	CVector *g = new CVector(pp);
	if (pp.dim == this->dim)
	{
		for (i = 0; i <= pp.dim - 1; i++)
		{
			g->v[i] += this->v[i];
		}
	}
	else
	{
		mistake = -1;
	}
	return *g;
}

double & CVector::operator*(const CVector &pp)
{
	double *g = new double;
	*g = 0;
	int i;
	if (this->dim == pp.dim)
	{
		for (i = 0; i <= pp.dim - 1; i++)
		{
			(*g) += pp.v[i]*(this->v[i]);
		}
	}
	else
	{
		mistake = -1;
	}
	return *g;
}

CVector & CVector::operator*(const double &k)
{
	CVector *g = new CVector(*this);
	int i;
	for (i = 0; i <= this->dim - 1; i++)
	{
		g->v[i] *= k;
	}
	return *g;
}
void CVector::print(void)
{
	int i;
	for (i = 0; i <= this->dim - 1; i++)
	{
		printf("%lf ", this->v[i]);
	}
	printf("\n");
}
