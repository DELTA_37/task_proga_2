#include "mycl.h"

int main(void)
{
	double mas[3] = {1, 2, 3};
	double mas2[3] = {2, 3, 4};
	CVector vect(3, mas);
	CVector v2(3, mas2);
	CVector v3;
	v3 = vect + v2;
	v3.print();
	
	return 0;
}
