#include "stdio.h"
#include "stdlib.h"

class CVector
{
private:
	int dim;
	double *v;
public:
	CVector(int);
	CVector(void);
	CVector(const CVector &);
	CVector(int, const double *);
	~CVector(void);
	void SetZero(void);
	void CopyOnly(const CVector &);
	void Clean(void);
	CVector & operator=(const CVector &);
	CVector & operator+(const CVector &);
	double & operator*(const CVector &);
	CVector & operator*(const double &);
	void print(void);
};
