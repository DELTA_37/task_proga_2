all: prog

prog: main.o mycl.o
	g++ main.o mycl.o -g -o prog
main.O: main.cpp
	g++ main.cpp -c -g -o main.o
mycl.o: mycl.cpp mycl.h
	g++ mycl.cpp -c -g -o mycl.o
clean:
	rm *.o
	rm prog
